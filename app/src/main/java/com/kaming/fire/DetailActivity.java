package com.kaming.fire;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class DetailActivity extends AppCompatActivity {

    private String mProduct_id = null;
    private DatabaseReference mDatabase;

    ImageView mDetailImg;
    TextView mDetailName,mDetailPrice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mDatabase = FirebaseDatabase.getInstance().getReference("product");
        mProduct_id = getIntent().getExtras().getString("product_id");

        mDetailName = (TextView) findViewById(R.id.detailName);
        mDetailPrice = (TextView) findViewById(R.id.detailPrice);
        mDetailImg = (ImageView) findViewById(R.id.detailImg);

        mDatabase.child(mProduct_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String product_name = (String) dataSnapshot.child("name").getValue();
                String product_uid = (String) dataSnapshot.child("id").getValue();
                String product_price = (String) dataSnapshot.child("price").getValue();
                String product_image = (String) dataSnapshot.child("image").getValue();

                mDetailName.setText(product_name);
                mDetailPrice.setText(product_price);
                Picasso.get().load(product_image).into(mDetailImg);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
